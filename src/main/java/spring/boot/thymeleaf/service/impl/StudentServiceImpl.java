package spring.boot.thymeleaf.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import spring.boot.thymeleaf.dao.entity.StudentEntity;
import spring.boot.thymeleaf.dao.repository.StudentRepository;
import spring.boot.thymeleaf.model.dto.StudentDto;
import spring.boot.thymeleaf.model.mapper.StudentMapper;
import spring.boot.thymeleaf.service.StudentService;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class StudentServiceImpl implements StudentService {
    private final StudentRepository service;

    @Override
    public void create(StudentDto studentDto) {
        StudentEntity entity = StudentMapper.MAPPER.toEntity(studentDto);
        service.save(entity);
        log.info("New user: " + studentDto.getName() + " " + studentDto.getSurname() + " added to DB");
    }

    @Override
    public Optional<StudentDto> getById(Integer id) {
        Optional<StudentEntity> entity = service.findById(id);
        if (entity.isEmpty()) {
            throw new NullPointerException("Student with this id is not exists");
        }
        log.info("Student with name: " + entity.get().getName() + " and surname: " + entity.get().getSurname() + " selected from DB");
        return entity.map(StudentMapper.MAPPER::toDto);
    }

    @Override
    public List<StudentDto> getAll() {
        if (service.findAll() == null) {
            throw new NullPointerException("DB is empty");
        }
        log.info("All students selected from DB");
        return service.findAll()
                .stream()
                .map(StudentMapper.MAPPER::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(Integer id) {
                    log.info("Student: " + service.findById(id).get().getName() + " " + service.findById(id).get().getSurname() + "successfully deleted");
            service.deleteById(id);
        }


}
