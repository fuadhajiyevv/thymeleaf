package spring.boot.thymeleaf.service;


import spring.boot.thymeleaf.model.dto.StudentDto;

import java.util.List;
import java.util.Optional;

public interface StudentService {

    void create(StudentDto studentDto);

    Optional<StudentDto> getById(Integer id);

    List<StudentDto> getAll();

    void deleteById(Integer Id);


}
