package spring.boot.thymeleaf.model.dto;

import lombok.Data;

@Data
public class StudentDto {
    private Integer id;
    private String name;
    private String surname;
    private Integer age;
    private String fin;
}
