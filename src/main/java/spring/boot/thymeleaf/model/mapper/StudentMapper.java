package spring.boot.thymeleaf.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import spring.boot.thymeleaf.dao.entity.StudentEntity;
import spring.boot.thymeleaf.model.dto.StudentDto;

@Mapper
public interface StudentMapper {

    StudentMapper MAPPER = Mappers.getMapper(StudentMapper.class);
    @Mapping(source = "studentDto.fin", target = "passport.fin")
    StudentEntity toEntity(StudentDto studentDto);
    @Mapping(source = "passport.fin",target = "fin")
    StudentDto toDto(StudentEntity student);
}
