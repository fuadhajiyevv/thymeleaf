package spring.boot.thymeleaf.dao.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "passport")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PassportEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "fin")
    private String fin;

    @Column(name = "passport_number")
    private Integer passportNumber;

    @OneToOne(mappedBy = "passport")
    private StudentEntity student;
}
