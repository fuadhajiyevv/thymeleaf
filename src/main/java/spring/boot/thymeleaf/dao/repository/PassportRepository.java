package spring.boot.thymeleaf.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.boot.thymeleaf.dao.entity.PassportEntity;

public interface PassportRepository extends JpaRepository<PassportEntity,Integer> {

}
