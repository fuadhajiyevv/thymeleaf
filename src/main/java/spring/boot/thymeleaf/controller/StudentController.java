package spring.boot.thymeleaf.controller;

import lombok.AllArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import spring.boot.thymeleaf.model.dto.StudentDto;
import spring.boot.thymeleaf.service.StudentService;


import java.util.Optional;

@Controller
@RequestMapping("/student")
@AllArgsConstructor
public class StudentController {
    private final StudentService studentService;

    @GetMapping("/")
    public String getAllStudents(Model model) {
        model.addAttribute("allStudentlist", studentService.getAll());
        return "index";
    }

    @GetMapping("/addnew")
    public String addNewStudent(Model model) {
        StudentDto student = new StudentDto();
        model.addAttribute("student", student);
        return "create";
    }

    @PostMapping("/save")
    public String saveStudent(@ModelAttribute("student") StudentDto studentDto) {
        studentService.create(studentDto);
        return "redirect:/student/";
    }

    @GetMapping("/showFormForUpdate/{id}")
    public String updateForm(@PathVariable(value = "id") int id, Model model) {
        Optional<StudentDto> student = studentService.getById(id);
        model.addAttribute("student", student.orElse(new StudentDto()));
        return "update";
    }

    @PostMapping("/update")
    public String updateStudent(@ModelAttribute("student") StudentDto studentDto) {
        studentService.create(studentDto);
        return "redirect:/student/";
    }
    @DeleteMapping("/deleteStudent/{id}")
    public String deleteThroughId(@PathVariable(value = "id") int id) {
        studentService.deleteById(id);
        return "redirect:/student/";
    }
    }

